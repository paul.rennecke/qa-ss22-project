# General Instructions

This project uses an openapi generator to generate API resource interfaces. To generate the interfaces
execute `./mvnw compile`. These interfaces can be extended to
implement the interfaces.
In order to run the project execute `./mvnw spring-boot:run`.
Make sure that all your tests are executed when calling `./mvnw verify`.

# Sonar [![Pipeline Status](https://git.rwth-aachen.de/paul.rennecke/qa-ss22-project/badges/master/pipeline.svg)](https://git.rwth-aachen.de/paul.rennecke/qa-ss22-project/-/commits/master) [![Quality Gate Status](https://sonar.paul-hoger.de/api/project_badges/measure?project=de.rwth.swc.sqa%3Asqa-ticketing&metric=alert_status&token=9e1b46a2b44b0ced3b373eca2c0111e635f524c8)](https://sonar.paul-hoger.de/dashboard?id=de.rwth.swc.sqa%3Asqa-ticketing)

In order to use sonar you have to install docker and docker-compose first. For Windows you can use Docker Desktop for
Windows.

Once docker is installed go to the directory `src/main/docker/sonar` and execute the command
`docker-compose up -d` to start sonarqube and the postgresql database. To stop sonar from running execute
`docker-compose stop` in the same directory.

After sonarqube is started access it on http://localhost:9000. You can login with admin admin. Set the new password to 'password'.

You can analyse your current build by executing `./mvnw sonar:sonar` after `./mvnw verify`.
