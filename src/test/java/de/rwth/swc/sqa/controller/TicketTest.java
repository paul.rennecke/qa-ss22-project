package de.rwth.swc.sqa.controller;

import de.rwth.swc.sqa.model.TicketRequest;
import io.restassured.RestAssured;
import io.swagger.models.auth.In;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class TicketTest {

    private static final String TICKETS_PATH = "/tickets";

    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
    }

    @Test
    void ticketInsert() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(201)
                .body("birthdate", equalTo(ticket.getBirthdate()))
                .body("validFrom", equalTo(ticket.getValidFrom()))
                .body("validFor", equalTo(ticket.getValidFor().toString()))
                .body("disabled", equalTo(false))
                .body("student", equalTo(false))
                .body("zone", equalTo(ticket.getZone().toString()));
    }

    @Test
    void ticketIncompleteRequest() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2002-05-20");
        ticket.zone(TicketRequest.ZoneEnum.A);

        given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(400);
    }


    @ParameterizedTest
    @CsvSource({"_1H,400",
            "_1D,400",
            "_30D,201",
            "_1Y,201"})
    void ticketStudentInvalidDurations(TicketRequest.ValidForEnum validFor, int expectedStatus) {

        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2022-05-20");
        ticket.student(true);
        ticket.zone(TicketRequest.ZoneEnum.A);

        ticket.validFor(validFor);
        given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    @ParameterizedTest
    @CsvSource({"abcd,400",
            "03.04.2000,400",
            "1910-03-04,201",
            "2004-3-12,400",
            "2010-13-20,400",
            "3000-01-01,201",
            "2010-40-01,400",
            "2010-03-31,201",
            "2010-03-32,400"})
    void ticketWithInvalidBirthdate(String givenBirthdate, int expectedStatus) {
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate(givenBirthdate);
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    @ParameterizedTest
    @CsvSource({"abcd,400",
            "03.04.2000,400",
            "2000-03-02,400",
            "1910-03-04T10:36:30,201",
            "1910-3-04T10:36:30,400",
            "1910-03-4T10:36:30,400",
            "1910-03-04-10:36:30,400",
            "1910-03-04T12:59:59,201",
            "1910-03-04T13:36:30,201",
            "1910-03-04T12:59:60,400",
            "1910-03-04T12:62:05,400",
            "1910-03-04T25:59:04,400",
            "2010-13-20T00:31:03,400",
            "3000-01-01T00:00:00,201",
            "2010-40-01T12:24:40,400",
            "2010-03-31T04:03:12,201",
            "2010-03-32T04:30:52,400",})
    void ticketWithInvalidValidFromDate(String givenDate, int expectedStatus) {
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom(givenDate);
        ticket.birthdate("2000-04-23");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    @ParameterizedTest
    @CsvSource({"25,201",
            "50,201",
            "33,400",
            "100,400"})
    void ticketWithInvalidDiscountCardValues(int givenValue, int expectedStatus) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);
        ticket.discountCard(givenValue);

        given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

}