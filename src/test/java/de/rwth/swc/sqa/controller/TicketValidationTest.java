package de.rwth.swc.sqa.controller;

import de.rwth.swc.sqa.model.*;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.threeten.extra.MutableClock;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class TicketValidationTest {

    private static final String TICKETS_PATH = "/tickets";
    private static final String VALIDATION_PATH = TICKETS_PATH + "/validate";
    private static final String CUSTOMERS_PATH = "/customers/";

    @LocalServerPort
    private int port;

    @Autowired
    private MutableClock clock;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
        RestAssured.defaultParser = Parser.JSON;
    }

    @Test
    void ticketValidationSuccessful() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._30D);
        ticket.zone(TicketRequest.ZoneEnum.C);


        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        TicketValidationRequest validation = new TicketValidationRequest();
        validation.disabled(false);
        validation.student(false);
        validation.date("2020-05-29T10:38:59");
        validation.ticketId(ticketID);
        validation.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validation)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    void ticketValidationForUnknownTicket() {
        TicketValidationRequest validation = new TicketValidationRequest();
        validation.disabled(false);
        validation.student(false);
        validation.date("2020-05-29T10:28:59");
        validation.ticketId(1290239032L);
        validation.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validation)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(403);
    }

    @Test
    void ticketValidationZoneInclusion() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);


        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        TicketValidationRequest validation = new TicketValidationRequest();
        validation.disabled(false);
        validation.date("2020-10-29T10:28:59");
        validation.ticketId(ticketID);
        validation.zone(TicketValidationRequest.ZoneEnum.A);

        given().header("Content-Type", "application/json")
                .body(validation)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    void ticketValidationWrongTime() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1H);
        ticket.zone(TicketRequest.ZoneEnum.C);

        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(false);
        validationRequest.date("2020-05-24T00:00:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.A);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(403);
    }

    @Test
    void ticketValidationStudentAge() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2030-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.student(true);
        ticket.validFor(TicketRequest.ValidForEnum._30D);
        ticket.zone(TicketRequest.ZoneEnum.C);

        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        // Check valid request
        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(true);
        validationRequest.date("2030-05-24T00:00:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(403);
    }

    @ParameterizedTest
    @CsvSource({"1h,403",
            "1d,200",
            "1y,200"})
    void ticketValidationSeniorAge(String validForEnum, int status) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2030-05-20T10:38:59");
        ticket.birthdate("1940-05-20");
        ticket.student(false);
        ticket.validFor(TicketRequest.ValidForEnum.fromValue(validForEnum));
        ticket.zone(TicketRequest.ZoneEnum.C);

        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        // Check valid request
        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(true);
        validationRequest.date("2030-05-20T10:40:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(status);
    }
    @ParameterizedTest
    @CsvSource({"1h,200",
            "1d,200",
            "1y,200",
            "30d,200"})
    void ticketValidationAdultAge(String validForEnum, int status) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2030-05-20T10:38:59");
        ticket.birthdate("1980-05-20");
        ticket.student(false);
        ticket.validFor(TicketRequest.ValidForEnum.fromValue(validForEnum));
        ticket.zone(TicketRequest.ZoneEnum.C);

        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        // Check valid request
        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(false);
        validationRequest.date("2030-05-20T10:40:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(status);
    }
    @ParameterizedTest
    @CsvSource({"1h,200",
            "1d,403",
            "1y,200",
            "30d,200"})
    void ticketValidationChildAge(String validForEnum, int status) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2030-05-20T10:38:59");
        ticket.birthdate("2020-05-20");
        ticket.student(false);
        ticket.validFor(TicketRequest.ValidForEnum.fromValue(validForEnum));
        ticket.zone(TicketRequest.ZoneEnum.C);

        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        // Check valid request
        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(false);
        validationRequest.date("2030-05-20T10:40:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(status);
    }

    @Test
    void ticketValidationNoGivenDiscountCard() {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._30D);
        ticket.zone(TicketRequest.ZoneEnum.C);
        ticket.discountCard(25);

        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(false);
        validationRequest.date("2020-05-24T00:00:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);


        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(403);
    }

    @Test
    void ticketValidationWithDiscountCard() {
        Customer customer = new Customer();
        customer.birthdate("2000-05-20");
        customer.disabled(false);

        Long customerId = given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        // Add a new discountcard to the previously created customer
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(customerId);
        discountCard.validFor(DiscountCard.ValidForEnum._1Y);
        discountCard.validFrom("2020-01-01");
        discountCard.type(25);

        Long discountCardId = given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-05-20T10:38:59");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._30D);
        ticket.zone(TicketRequest.ZoneEnum.C);
        ticket.discountCard(25);

        Long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        // Check valid request
        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(false);
        validationRequest.date("2020-05-24T00:00:00");
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);
        validationRequest.discountCardId(discountCardId);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(200);
    }

    @ParameterizedTest
    @CsvSource({"2019-05-20T10:00:00, _30D ,2020-01-01, _1Y, 2019-05-27T10:00:00, 403",
            "2019-05-20T10:00:00, _30D, 2019-05-20, _1Y, 2019-05-27T10:00:00, 200",
            "2019-05-20T10:00:00, _30D ,2019-05-19, _1Y, 2019-05-27T10:00:00, 200",
            "2019-05-20T10:00:00, _30D ,2018-09-01, _1Y, 2019-05-27T10:00:00, 200",
            "2019-05-20T10:00:00, _30D ,2017-01-01, _1Y, 2019-05-27T10:00:00, 403",
            "2019-05-20T10:00:00, _30D ,2018-05-21, _1Y, 2019-05-27T10:00:00, 403",
            "2019-05-20T10:00:00, _30D ,2018-05-27, _1Y, 2019-05-27T10:00:00, 403",
            "2019-04-28T10:00:00, _30D ,2018-05-28, _1Y, 2019-04-28T90:00:00, 403",
            "2019-04-28T10:00:00, _30D ,2018-05-28, _1Y, 2019-04-28T10:00:00, 200",
            "2019-04-28T10:00:00, _30D ,2018-05-28, _1Y, 2019-05-28T10:00:00, 200",
            "2019-04-28T10:00:00, _30D ,2018-05-28, _1Y, 2019-05-29T10:00:00, 403",
            "2019-05-20T10:00:00, _30D ,2019-05-28, _30D, 2019-05-30T10:00:00, 403",
            "2019-05-28T10:00:00, _30D ,2019-05-20, _30D, 2019-05-30T10:00:00, 403",
            "2019-05-20T10:00:00, _1Y ,2019-05-28, _30D, 2019-05-30T01:00:00, 403",
            "2019-05-20T00:00:00, _30D ,2019-05-20, _1Y, 2019-05-30T01:00:00, 200",
            "2019-05-20T00:00:00, _30D ,2018-06-18, _1Y, 2019-05-30T01:00:00, 200",
            "2019-05-20T00:00:00, _30D ,2018-06-17, _1Y, 2019-05-30T01:00:00, 403"})
    void ticketValidationDiscountCardOnInvalidDate(String ticketStart, TicketRequest.ValidForEnum ticketDuration,
                                                   String discountCardStart, DiscountCard.ValidForEnum discountCardDuration,
                                                   String validationDate, int statusCode) {
        Customer customer = new Customer();
        customer.birthdate("2000-05-20");
        customer.disabled(false);

        Long customerId = given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        // Add a new discountcard to the previously created customer
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(customerId);
        discountCard.validFrom(discountCardStart);
        discountCard.validFor(discountCardDuration);
        discountCard.type(25);

        Long discountCardId = given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom(ticketStart);
        ticket.birthdate("2000-05-20");
        ticket.validFor(ticketDuration);
        ticket.zone(TicketRequest.ZoneEnum.C);
        ticket.discountCard(25);

        Long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .assertThat()
                .statusCode(201)
                .extract().path("id");

        // Check valid request
        TicketValidationRequest validationRequest = new TicketValidationRequest();
        validationRequest.disabled(false);
        validationRequest.student(false);
        validationRequest.date(validationDate);
        validationRequest.ticketId(ticketID);
        validationRequest.zone(TicketValidationRequest.ZoneEnum.C);
        validationRequest.discountCardId(discountCardId);

        given().header("Content-Type", "application/json")
                .body(validationRequest)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(statusCode);
    }

    @ParameterizedTest
    @CsvSource({"abcd,403",
            "03.04.2020,403",
            "2020-03-02,403",
            "2020-03-04T10:36:30,200",
            "2020-3-04T10:36:30,403",
            "2020-03-4T10:36:30,403",
            "2020-03-04-10:36:30,403",
            "2020-03-04T12:59:59,200",
            "2020-03-04T13:36:30,200",
            "2020-03-04T12:59:60,403",
            "2020-03-04T12:62:05,403",
            "2020-03-04T25:59:04,403",
            "2020-13-20T00:31:03,403",
            "3000-01-01T00:00:00,403",
            "2020-40-01T12:24:40,403",
            "2020-03-31T04:03:12,200",
            "2020-03-32T04:30:52,403",
            // Border values for validity
            "2020-01-01T00:00:00,403",
            "2020-01-01T00:00:01,200",
            "2021-01-01T00:00:01,200",
            "2021-01-01T00:00:02,403"})
    void ticketValidationWithInvalidDate(String givenDate, int expectedStatus) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-01-01T00:00:01");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);


        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        TicketValidationRequest validation = new TicketValidationRequest();
        validation.disabled(false);
        validation.student(false);
        validation.date(givenDate);
        validation.ticketId(ticketID);
        validation.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validation)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    @ParameterizedTest
    @CsvSource({"true,true,200",
            "true,false,403",
            "false,true,200",
            "false,false,200"})
    void ticketValidationDisabilityMismatch(boolean ticketValue, boolean validationValue, int expectedStatus) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-01-01T00:00:01");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);
        ticket.disabled(ticketValue);


        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        TicketValidationRequest validation = new TicketValidationRequest();
        validation.disabled(validationValue);
        validation.student(false);
        validation.date("2020-06-01T00:00:01");
        validation.ticketId(ticketID);
        validation.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validation)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    @ParameterizedTest
    @CsvSource({"true,true,200",
            "true,false,403",
            "false,true,200",
            "false,false,200"})
    void ticketValidationStudentMismatch(boolean ticketValue, boolean validationValue, int expectedStatus) {
        // Create a new ticket
        TicketRequest ticket = new TicketRequest();
        ticket.validFrom("2020-01-01T00:00:01");
        ticket.birthdate("2000-05-20");
        ticket.validFor(TicketRequest.ValidForEnum._1Y);
        ticket.zone(TicketRequest.ZoneEnum.C);
        ticket.student(ticketValue);


        long ticketID = given().header("Content-Type", "application/json")
                .body(ticket)
                .post(TICKETS_PATH).then()
                .extract().path("id");

        TicketValidationRequest validation = new TicketValidationRequest();
        validation.disabled(false);
        validation.student(validationValue);
        validation.date("2020-06-01T00:00:01");
        validation.ticketId(ticketID);
        validation.zone(TicketValidationRequest.ZoneEnum.C);

        given().header("Content-Type", "application/json")
                .body(validation)
                .post(VALIDATION_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    // Check if valid from is in the future

}