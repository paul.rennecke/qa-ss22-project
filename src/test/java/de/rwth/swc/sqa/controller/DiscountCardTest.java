package de.rwth.swc.sqa.controller;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class DiscountCardTest {
    private static final String CUSTOMERS_PATH = "/customers/";

    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
    }

    @Test
    void discountCardAddToCustomer() {
        // Create a new customer
        long customerId = getNewCustomer();

        // Add a new discountcard to the previously created customer
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(customerId);
        discountCard.validFor(DiscountCard.ValidForEnum._1Y);
        discountCard.validFrom("2022-01-01");
        discountCard.type(25);

        given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards");

        given().pathParam("customerId", customerId)
                .get(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(200)
                .body("", hasItem(allOf(hasEntry("customerId", customerId))));
    }

    @Test
    void newCustomerNoCardFound() {
        long customerId = getNewCustomer();

        given().pathParam("customerId", customerId)
                .get(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(404);
    }

    @Test
    void testDiscountcardSortOrder() {
        long customerId = getNewCustomer();

        // Add a new discountcard to the previously created customer
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(customerId);
        discountCard.validFor(DiscountCard.ValidForEnum._30D);
        discountCard.validFrom("2022-01-01");
        discountCard.type(25);

        DiscountCard discountCard2 = new DiscountCard();
        discountCard2.customerId(customerId);
        discountCard2.validFor(DiscountCard.ValidForEnum._30D);
        discountCard2.validFrom("2022-02-01");
        discountCard2.type(25);

        DiscountCard discountCard3 = new DiscountCard();
        discountCard3.customerId(customerId);
        discountCard3.validFor(DiscountCard.ValidForEnum._30D);
        discountCard3.validFrom("2022-04-03");
        discountCard3.type(25);

        given().header("Content-Type", "application/json")
                .body(discountCard3)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards");
        given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards");
        given().header("Content-Type", "application/json")
                .body(discountCard2)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards");

        System.out.println(given().pathParam("customerId", customerId)
                .get(CUSTOMERS_PATH + "{customerId}/discountcards").body().asString());

        given().pathParam("customerId", customerId)
                .get(CUSTOMERS_PATH + "{customerId}/discountcards")
                .then().assertThat()
                .body("validFrom", containsInRelativeOrder(discountCard.getValidFrom(), discountCard2.getValidFrom(), discountCard3.getValidFrom()));
    }

    @Test
    void discountCardWrongCustomerId() {
        given().pathParam("customerId", 498598409)
                .get(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(404);
    }

    @Test
    void discountCardPostWrongCustomerId() {
        // Add a new discountcard to the previously created customer
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(294809384039L);
        discountCard.validFor(DiscountCard.ValidForEnum._1Y);
        discountCard.validFrom("2022-01-01");
        discountCard.type(25);

        given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", 294809384039L)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(404);
    }

    @Test
    void discountCardInvalidType() {
        // Create a new customer
        long customerId = getNewCustomer();

        // Invalid Discount Card Type
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(customerId);
        discountCard.validFor(DiscountCard.ValidForEnum._1Y);
        discountCard.validFrom("2022-01-01");
        discountCard.type(90);

        given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(400);
    }

    @ParameterizedTest
    @CsvSource({"2022-01-01,30d,2022-06-01,30d,201",
                "2022-01-01,1y,2022-06-01,30d,409",
                "2022-01-01,30d,2022-06-01,1y,201",
                "2022-01-01,30d,2022-02-01,30d,201",
                "2022-01-01,30d,2022-01-20,30d,409",
                "2022-06-01,30d,2022-01-20,1y,409",
                "2022-06-01,30d,2022-06-01,30d,409",
                })
    void discountCardWithDuplicatedTimeRange(String firstDate, String validForFirst, String secondDate, String validForSecond, int statusCode) {
        long customerId = getNewCustomer();

        DiscountCard card1 = new DiscountCard();
        card1.validFor(DiscountCard.ValidForEnum.fromValue(validForFirst));
        card1.validFrom(firstDate);
        card1.setCustomerId(customerId);
        card1.type(25);

        DiscountCard card2 = new DiscountCard();
        card2.validFor(DiscountCard.ValidForEnum.fromValue(validForSecond));
        card2.validFrom(secondDate);
        card2.setCustomerId(customerId);
        card2.type(25);

        given().header("Content-Type", "application/json")
                .body(card1)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards");

        given().header("Content-Type", "application/json")
                .body(card2)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(statusCode);
    }

    @ParameterizedTest
    @CsvSource({"abcd,400",
            "03.04.2000,400",
            "1910-03-04,201",
            "2004-3-12,400",
            "2010-13-20,400",
            "3000-01-01,201",
            "2010-40-01,400",
            "2010-03-31,201",
            "2010-03-32,400"})
    void discountCardWithInvalidValidFromDate(String givenDate, int expectedStatus) {
        // Create a new customer
        long customerId = getNewCustomer();

        // Add a new discountcard to the previously created customer
        DiscountCard discountCard = new DiscountCard();
        discountCard.customerId(customerId);
        discountCard.validFor(DiscountCard.ValidForEnum._1Y);
        discountCard.validFrom(givenDate);
        discountCard.type(25);

        given().header("Content-Type", "application/json")
                .body(discountCard)
                .pathParam("customerId", customerId)
                .post(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                .assertThat()
                .statusCode(expectedStatus);

        if (expectedStatus == 201) {
            given().pathParam("customerId", customerId)
                    .get(CUSTOMERS_PATH + "{customerId}/discountcards").then()
                    .assertThat()
                    .statusCode(200)
                    .body("", hasItem(allOf(hasEntry("customerId", customerId))));
        }
    }

    private long getNewCustomer() {
        Customer customer = new Customer();
        customer.birthdate("2000-05-20");

        return given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .extract().path("id");
    }
}