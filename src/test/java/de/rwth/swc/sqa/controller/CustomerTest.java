package de.rwth.swc.sqa.controller;

import de.rwth.swc.sqa.model.Customer;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class CustomerTest {
    private static final String CUSTOMERS_PATH = "/customers/";

    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
    }

    @Test
    void customerInsert() {
        // Create a new customer
        Customer customer = new Customer();
        customer.birthdate("2000-05-20");

        given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(201)
                .body("birthdate", equalTo(customer.getBirthdate()))
                .body("disabled", equalTo(false))
                .extract().path("id");
    }

    @Test
    void customerIncompleteInput() {
        // Create a new customer
        Customer customer = new Customer();
        customer.disabled(true);

        given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(400);
    }

    @ParameterizedTest
    @CsvSource({"abcd,400",
            "03.04.2000,400",
            "1910-03-04,201",
            "2004-3-12,400",
            "2010-13-20,400",
            "3000-01-01,201",
            "2010-40-01,400",
            "2010-03-31,201",
            "2010-03-32,400"})
    void customerWithInvalidBirthdate(String givenBirthdate, int expectedStatus) {
        // Create a new customer
        Customer customer = new Customer();
        customer.birthdate(givenBirthdate);

        given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(expectedStatus);
    }

    @Test
    void customerInsertWithExistingId() {
        // Create a new customer
        Customer customer = new Customer();
        customer.birthdate("2000-05-20");
        customer.id(13L);

        given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(201)
                .body("birthdate", equalTo(customer.getBirthdate()))
                .body("disabled", equalTo(false));

        Customer customer2 = new Customer();
        customer2.birthdate("2010-05-20");
        customer2.id(13L);

        given().header("Content-Type", "application/json")
                .body(customer)
                .post(CUSTOMERS_PATH).then()
                .assertThat()
                .statusCode(400);
    }

}