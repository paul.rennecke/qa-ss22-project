package de.rwth.swc.sqa.database.exception;

public class InvalidInputException extends Exception {

    public InvalidInputException(String msg) {
        super(msg);
    }
}
