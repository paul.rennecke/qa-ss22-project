package de.rwth.swc.sqa.database;

import de.rwth.swc.sqa.database.exception.ObjectNotFoundException;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.util.DateUtil;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
public class Database {

    private final Map<Long, Customer> customers;
    private final Map<Long, List<DiscountCard>> discountCards;

    private final Map<Long, Ticket> tickets;

    public Database() {
        this.customers = new HashMap<>();
        this.discountCards = new HashMap<>();
        this.tickets = new HashMap<>();
    }

    public void addCustomer(Customer customer) {
        customers.put(customer.getId(), customer);
    }

    public DiscountCard getDiscountCardById(Long id) {
        for (Map.Entry<Long, List<DiscountCard>> entry : discountCards.entrySet()) {
            for (DiscountCard tmp : entry.getValue()) {
                if (tmp.getId().equals(id)) {
                    return tmp;
                }
            }
        }
        return null;
    }

    public Customer getCustomerById(long id) {
        return customers.get(id);
    }

    public Ticket getTicketById(long id) {
        return tickets.get(id);
    }

    public void addDiscountCard(long customerId, DiscountCard discountCard) throws ObjectNotFoundException {
        if (!customers.containsKey(customerId)) {
            throw new ObjectNotFoundException();
        }
        if (discountCards.containsKey(customerId)) {
            discountCards.get(customerId).add(discountCard);
        } else {
            List<DiscountCard> tmp = new ArrayList<>();
            tmp.add(discountCard);
            discountCards.put(customerId, tmp);
        }
    }

    public List<DiscountCard> getDiscountCards(long customerId) throws ObjectNotFoundException {
        if (!customers.containsKey(customerId)) {
            throw new ObjectNotFoundException();
        }
        List<DiscountCard> cards = discountCards.containsKey(customerId) ? discountCards.get(customerId) : new ArrayList<>();
        cards.sort((o1, o2) -> {
            LocalDate validFrom1 = LocalDate.parse(o1.getValidFrom(), DateUtil.formatterWithoutTime());
            LocalDate validFrom2 = LocalDate.parse(o2.getValidFrom(), DateUtil.formatterWithoutTime());
            if (validFrom1.isEqual(validFrom2)) {
                return 0;
            }
            if (validFrom1.isBefore(validFrom2)) {
                return -1;
            } else {
                return 1;
            }
        });

        return cards;
    }

    public void addTicket(Ticket ticket) {
        tickets.put(ticket.getId(), ticket);
    }
}