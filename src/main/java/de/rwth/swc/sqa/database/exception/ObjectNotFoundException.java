package de.rwth.swc.sqa.database.exception;

public class ObjectNotFoundException extends Exception {

    public ObjectNotFoundException() {
        super("Object not found");
    }
}
