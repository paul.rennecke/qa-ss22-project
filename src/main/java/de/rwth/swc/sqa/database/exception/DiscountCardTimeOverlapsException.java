package de.rwth.swc.sqa.database.exception;

public class DiscountCardTimeOverlapsException extends Exception {

    public DiscountCardTimeOverlapsException() {
        super("Discountcard time overlaps");
    }
}
