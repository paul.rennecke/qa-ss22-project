package de.rwth.swc.sqa.util;

import de.rwth.swc.sqa.database.exception.InvalidInputException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateUtil {

    private DateUtil() {

    }

    public static DateTimeFormatter formatterWithoutTime() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }

    public static DateTimeFormatter formatterWithTime() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    }

    public static void checkValidDateWithoutTime(String time) throws InvalidInputException {
        try {
            LocalDate.parse(time, formatterWithoutTime());
        } catch (DateTimeParseException e) {
            throw new InvalidInputException("Invalid date");
        }
    }

    public static void checkValidDateWithTime(String time) throws InvalidInputException {
        try {
            LocalDate.parse(time, formatterWithTime());
        } catch (DateTimeParseException e) {
            throw new InvalidInputException("Invalid date");
        }
    }
}
