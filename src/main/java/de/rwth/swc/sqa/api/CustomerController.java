package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.database.exception.DiscountCardTimeOverlapsException;
import de.rwth.swc.sqa.database.exception.ObjectNotFoundException;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.service.customer.CustomerService;
import de.rwth.swc.sqa.service.customer.DiscountCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CustomerController implements CustomersApi {

    private final CustomerService customerService;
    private final DiscountCardService discountCardService;

    @Autowired
    public CustomerController(CustomerService customerService, DiscountCardService discountCardService) {
        this.customerService = customerService;
        this.discountCardService = discountCardService;
    }

    @Override
    public ResponseEntity<Customer> addCustomer(Customer body) {
        try {
            return ResponseEntity.status(201).body(customerService.addCustomer(body));
        } catch (Exception e) {
            return ResponseEntity.status(400).build();
        }
    }

    @Override
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard body) {
        try {
            return ResponseEntity.status(201).body(discountCardService.addDiscountCardToCustomer(customerId, body));
        } catch (ObjectNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (DiscountCardTimeOverlapsException e) {
            return ResponseEntity.status(409).build();
        } catch (Exception e) {
            return ResponseEntity.status(400).build();
        }
    }

    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        if (customerId == null) {
            return ResponseEntity.status(400).build();
        }
        try {
            return ResponseEntity.ok(discountCardService.getDiscountCardsFromCustomer(customerId));
        } catch (ObjectNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
