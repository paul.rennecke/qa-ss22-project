package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import de.rwth.swc.sqa.service.tickets.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class TicketController implements TicketsApi {

    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public ResponseEntity<Ticket> buyTicket(TicketRequest body) {
        try {
            return ResponseEntity.status(201).body(ticketService.buyTicket(body));
        } catch (Exception e) {
            return ResponseEntity.status(400).build();
        }
    }

    @Override
    public ResponseEntity<Void> validateTicket(TicketValidationRequest body) {
        try {
            if (ticketService.validateTicket(body)) {
                return ResponseEntity.status(200).build();
            } else {
                return ResponseEntity.status(403).build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(400).build();
        }
    }
}
