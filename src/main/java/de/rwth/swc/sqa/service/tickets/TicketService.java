package de.rwth.swc.sqa.service.tickets;

import de.rwth.swc.sqa.database.Database;
import de.rwth.swc.sqa.database.exception.InvalidInputException;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import de.rwth.swc.sqa.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class TicketService {

    private final Database database;

    @Autowired
    public TicketService(Database database) {
        this.database = database;
    }

    public Ticket buyTicket(TicketRequest ticketRequest) throws InvalidInputException {
        Ticket ticket = new Ticket();
        if (ticketRequest.getDisabled() == null) {
            ticket.disabled(false);
        } else {
            ticket.disabled(ticketRequest.getDisabled());
        }
        if (ticketRequest.getStudent() == null) {
            ticket.student(false);
        } else {
            ticket.student(ticketRequest.getStudent());
        }
        if (ticketRequest.getDiscountCard() == null) {
            ticket.discountCard(0);
        } else {
            if (!List.of(0, 25, 50).contains(ticketRequest.getDiscountCard())) {
                throw new InvalidInputException("Invalid discountcard type: " + ticketRequest.getDiscountCard());
            }
            ticket.discountCard(ticketRequest.getDiscountCard());
        }
        ticket.validFrom(ticketRequest.getValidFrom());
        ticket.birthdate(ticketRequest.getBirthdate());
        ticket.validFor(de.rwth.swc.sqa.model.Ticket.ValidForEnum.fromValue(ticketRequest.getValidFor().getValue()));
        if (ticketRequest.getValidFor() == TicketRequest.ValidForEnum._1H) {
            if (ticketRequest.getZone() == null) {
                throw new InvalidInputException("Zone required");
            } else {
                ticket.zone(Ticket.ZoneEnum.fromValue(ticketRequest.getZone().getValue()));
            }
        } else {
            ticket.zone(Ticket.ZoneEnum.C);
        }
        ticket.id(ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE));

        validateInput(ticket);

        database.addTicket(ticket);

        return ticket;
    }

    private void validateInput(Ticket ticket) throws InvalidInputException {
        DateUtil.checkValidDateWithTime(ticket.getValidFrom());
        DateUtil.checkValidDateWithoutTime(ticket.getBirthdate());

        if (ticket.getStudent() && !(ticket.getValidFor() == Ticket.ValidForEnum._1Y || ticket.getValidFor() == Ticket.ValidForEnum._30D)) {
            throw new InvalidInputException("");
        }
    }

    public boolean validateTicket(TicketValidationRequest validationRequest) {
        Ticket ticket = database.getTicketById(validationRequest.getTicketId());
        // Check if referenced ticket exists
        if (ticket == null) {
            return false;
        }

        if (ticket.getDisabled() && Boolean.FALSE.equals(validationRequest.getDisabled())) {
            return false;
        }

        if (ticket.getStudent() && Boolean.FALSE.equals(validationRequest.getStudent())) {
            return false;
        }

        try {
            DateUtil.checkValidDateWithTime(validationRequest.getDate());
        } catch (InvalidInputException e) {
            return false;
        }
        LocalDateTime validationDate = LocalDateTime.parse(validationRequest.getDate(), DateUtil.formatterWithTime());

        DiscountCard discountCard = database.getDiscountCardById(validationRequest.getDiscountCardId());

        return checkAge(ticket, validationDate) && checkTicketTime(ticket, validationDate) && checkDiscountCard(ticket, discountCard);
    }

    private boolean checkAge(Ticket ticket, LocalDateTime validationDate) {
        LocalDate birthdayDate = LocalDate.parse(ticket.getBirthdate(), DateUtil.formatterWithoutTime());
        int currentAge = (int) ChronoUnit.YEARS.between(birthdayDate, validationDate);

        if (currentAge >= 28 && ticket.getStudent()) {
            return false;
        }

        // Senior
        if (currentAge >= 60 && ticket.getValidFor() == Ticket.ValidForEnum._1H) {
            return false;
        }
        // Student
        if (currentAge < 28
                && ticket.getStudent()
                && !(ticket.getValidFor() == Ticket.ValidForEnum._30D || ticket.getValidFor() == Ticket.ValidForEnum._1Y)) {
            return false;
        }
        // Child
        return currentAge >= 14 || ticket.getValidFor() != Ticket.ValidForEnum._1D;
    }

    private boolean checkTicketTime(Ticket ticket, LocalDateTime validationDate) {
        // Time check for Ticket
        LocalDateTime ticketStart = getTicketStart(ticket);
        LocalDateTime ticketEnd = getTicketEnd(ticket);

        return ChronoUnit.SECONDS.between(ticketEnd, validationDate) <= 0
                && ChronoUnit.SECONDS.between(ticketStart, validationDate) >= 0;
    }

    private boolean checkDiscountCard(Ticket ticket, DiscountCard discountCard) {
        if (ticket.getDiscountCard() == 0) {
            return true;
        }

        if (discountCard == null) {
            return false;
        }

        if (!ticket.getDiscountCard().equals(discountCard.getType())) {
            return false;
        }

        discountCard.validFrom(discountCard.getValidFrom() + "T00:00:00");

        LocalDateTime ticketStart = getTicketStart(ticket);
        LocalDateTime ticketEnd = getTicketEnd(ticket);

        // Time check for Ticket
        LocalDateTime discountCardValidStart = LocalDateTime.parse(discountCard.getValidFrom(), DateUtil.formatterWithTime());
        LocalDateTime discountCardValidEnd = switch (discountCard.getValidFor()) {
            case _1Y -> discountCardValidStart.plusYears(1);
            case _30D -> discountCardValidStart.plusDays(30);
        };
        discountCardValidEnd = discountCardValidEnd.plusDays(1);

        return ChronoUnit.SECONDS.between(ticketEnd, discountCardValidEnd) >= 0
                && ChronoUnit.SECONDS.between(ticketStart, discountCardValidStart) <= 0;
    }

    private LocalDateTime getTicketStart(Ticket ticket) {
        return LocalDateTime.parse(ticket.getValidFrom(), DateUtil.formatterWithTime());
    }

    private LocalDateTime getTicketEnd(Ticket ticket) {
        LocalDateTime ticketStart = getTicketStart(ticket);

        return switch (ticket.getValidFor()) {
            case _1Y -> ticketStart.plusYears(1);
            case _30D -> ticketStart.plusDays(30);
            case _1D -> ticketStart.plusDays(1);
            case _1H -> ticketStart.plus(1, ChronoUnit.HOURS);
        };
    }
}
