package de.rwth.swc.sqa.service.customer;

import de.rwth.swc.sqa.database.Database;
import de.rwth.swc.sqa.database.exception.DiscountCardTimeOverlapsException;
import de.rwth.swc.sqa.database.exception.InvalidInputException;
import de.rwth.swc.sqa.database.exception.ObjectNotFoundException;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class DiscountCardService {

    private final Database database;

    @Autowired
    public DiscountCardService(Database database) {
        this.database = database;
    }

    public DiscountCard addDiscountCardToCustomer(long customerId, DiscountCard body) throws ObjectNotFoundException, DiscountCardTimeOverlapsException, InvalidInputException {
        DiscountCard discountCard = new DiscountCard();
        discountCard.id(ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE));
        discountCard.customerId(customerId);
        if (!List.of(25, 50).contains(body.getType())) {
            throw new InvalidInputException("Invalid discountcard type: " + body.getType());
        }
        discountCard.type(body.getType());
        discountCard.validFrom(body.getValidFrom());
        discountCard.validFor(body.getValidFor());

        checkValid(discountCard);
        checkIfTimeframeIsValid(discountCard, customerId);

        database.addDiscountCard(customerId, discountCard);

        return discountCard;
    }

    public List<DiscountCard> getDiscountCardsFromCustomer(long customerId) throws ObjectNotFoundException {
        List<DiscountCard> res = database.getDiscountCards(customerId);
        // Throw 404 if no discount card exists
        if (res.isEmpty()) {
            throw new ObjectNotFoundException();
        }
        return res;
    }

    private void checkIfTimeframeIsValid(DiscountCard discountCard, long customerId) throws ObjectNotFoundException, DiscountCardTimeOverlapsException {
        Long[] newDiscountCardDate = getDateTimeFromDiscountCard(discountCard);
        for (DiscountCard customerDiscountCard : database.getDiscountCards(customerId)) {
            Long[] customerDiscountCardDate = getDateTimeFromDiscountCard(customerDiscountCard);
            if (newDiscountCardDate[0] <= customerDiscountCardDate[1] && newDiscountCardDate[1] >= customerDiscountCardDate[0]) {
                throw new DiscountCardTimeOverlapsException();
            }
        }
    }

    private Long[] getDateTimeFromDiscountCard(DiscountCard discountCard) {
        Long[] startEnd = new Long[2];
        LocalDate dateTime = LocalDate.parse(discountCard.getValidFrom(), DateUtil.formatterWithoutTime());
        startEnd[0] = dateTime.toEpochDay();

        if (discountCard.getValidFor() == DiscountCard.ValidForEnum._1Y) {
            dateTime = dateTime.plusYears(1);
        } else {
            dateTime = dateTime.plusDays(30);
        }
        startEnd[1] = dateTime.toEpochDay();

        return startEnd;
    }

    private void checkValid(DiscountCard discountCard) throws InvalidInputException {
        List<Integer> validTypes = List.of(25, 50);
        if (!validTypes.contains(discountCard.getType())) {
            throw new InvalidInputException("Wrong type: must be 25, 50");
        }

        DateUtil.checkValidDateWithoutTime(discountCard.getValidFrom());
    }
}
