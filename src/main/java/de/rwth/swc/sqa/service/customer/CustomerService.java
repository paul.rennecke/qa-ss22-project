package de.rwth.swc.sqa.service.customer;

import de.rwth.swc.sqa.database.Database;
import de.rwth.swc.sqa.database.exception.InvalidInputException;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class CustomerService {

    private final Database database;

    @Autowired
    public CustomerService(Database database) {
        this.database = database;
    }

    public Customer addCustomer(Customer body) throws InvalidInputException {

        DateUtil.checkValidDateWithoutTime(body.getBirthdate());

        Customer customer = new Customer();
        if (body.getId() != null) {
            if (database.getCustomerById(body.getId()) == null) {
                customer.id(body.getId());
            } else {
                throw new InvalidInputException("Customer ID already taken");
            }
        } else {
            customer.id(ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE));
        }
        customer.birthdate(body.getBirthdate());
        if (body.getDisabled() == null) {
            customer.disabled(false);
        } else {
            customer.disabled(body.getDisabled());
        }

        database.addCustomer(customer);

        return customer;
    }
}
